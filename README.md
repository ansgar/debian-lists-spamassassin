# Listmaster/Debbugs Spamassassin Configuration #

the bugs/ directory contains the debbugs user_prefs file

the list/ directory contains the l.d.o user_prefs file (and later will
contain amavis policy file(s))

put rules into common/ and symlink common into .spamassassin from
wherever you checkout this setup.

# Automatic Updates #

This repository automatically updates itself if the tip commit is signed.

Currently, the keyring is manually maintained on buxtehude with the
following commands:

``` shell
( for a in $(getent group debbugs|awk -F: '{print $4}'|sed 's/,/ /g'); do 
  finger $a/key@db.debian.org; done; )|sudo -u debbugs -H gpg --import
( for a in $(getent group list|awk -F: '{print $4}'|sed 's/,/ /g'); do 
  finger $a/key@db.debian.org; done; )|sudo -u debbugs -H gpg --import
```

and on bendel:

``` shell
( for a in $(getent group debbugs|awk -F: '{print $4}'|sed 's/,/ /g'); do 
  finger $a/key@db.debian.org; done; )|sudo -u list -H gpg --import
( for a in $(getent group list|awk -F: '{print $4}'|sed 's/,/ /g'); do 
  finger $a/key@db.debian.org; done; )|sudo -u list -H gpg --import
```
